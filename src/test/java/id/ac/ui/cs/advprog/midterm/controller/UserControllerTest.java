package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.UserRepository;
import id.ac.ui.cs.advprog.midterm.core.User;
import id.ac.ui.cs.advprog.midterm.core.UserWithDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.assertj.core.api.Assertions.*;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Mock
    private UserWithDTO user_dto;

    @Mock
    private User user;
    private static String id = "1";

    @Mock
    private BindingResult result;

    @Mock
    private Model model;

    @MockBean
    private UserRepository userRepository;

    private static final String ID_NOT_FOUND = "Invalid user Id:1";

    @BeforeEach
    public void setUp() {
        user = new User();
        user.setId(1);
        id = Long.toString(user.getId());
        user.setEmail("email@email.com");
        user.setName("AdvProgMaster");
        when(userRepository.findById(user.getId())).thenReturn(java.util.Optional.ofNullable(user));

        UserWithDTO user_dto = new UserWithDTO();
        user_dto.setEmail(user.getEmail());
        user_dto.setId(user.getId());
        user_dto.setName(user.getName());
        when(userRepository.findById(user_dto.getId())).thenReturn(java.util.Optional.ofNullable(user_dto));
    }

    // URL Routing, HTTP Response and Template Tests
    @Test
    public void testControllerSignupTemplate() throws Exception {
        mockMvc.perform(get("/signup")).andExpect(status().isOk()).andExpect(view().name("add-user"));
    }

    @Test
    public void testControllerIndexTemplate() throws Exception {
        mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(view().name("index"));
    }

    @Test
    public void testControllerAddUserTemplate() throws Exception {
        mockMvc.perform(post("/adduser")).andExpect(status().isOk()).andExpect(view().name("index"));
    }

    @Test
    public void testControllerEditTemplateGET() throws Exception {
        try{
            mockMvc.perform(get("/edit/" + id)).andExpect(status().isOk()).andExpect(view().name("update-user"));
        }catch (Exception e){
            assertThat(e).hasMessageContaining(ID_NOT_FOUND);
        }
    }

    @Test
    public void testControllerEditTemplateExist() throws Exception {
        try{
            mockMvc.perform(post("/edit/" + id));
        }catch (Exception e){
            assertThat(e).hasMessageContaining(ID_NOT_FOUND);
        }
    }

    @Test
    public void testControllerUpdateTemplateGET() throws Exception {
        mockMvc.perform(get("/update/" + id)).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void testControllerUpdateTemplatePOST() throws Exception {
        HashMap newUser = new HashMap();
        newUser.put("user", user);
        newUser.put("result", result);
        newUser.put("model", model);
        mockMvc.perform(post("/update/" + id)).andExpect(status().isOk()).andExpect(view().name("index"));
    }

    @Test
    public void testDeleteUser() throws Exception{
        try{
            mockMvc.perform(get("/delete/" + id));
        }catch (Exception e){
            assertThat(e).hasMessageContaining(ID_NOT_FOUND);
        }
    }

    //Testing POST Requests
    @Test
    public void testAddUserValid() throws Exception{
        //Testing POST Request
        HashMap newUser = new HashMap();
        newUser.put("user",user_dto);
        newUser.put("model",model);
        newUser.put("result",result);
        mockMvc.perform(post("/adduser").flashAttrs(newUser)).andExpect(status().isOk()).andExpect(view().name("index"));
    }

    @Test
    public void testAddUserValidNullName() throws Exception{
        user_dto.setName(null);
        HashMap newUser = new HashMap();
        newUser.put("user",user_dto);
        newUser.put("model",model);
        newUser.put("result",result);
        mockMvc.perform(post("/adduser").flashAttrs(newUser)).andExpect(status().isOk()).andExpect(view().name("index"));
    }

    @Test
    public void testAddUserValidNullEmail() throws Exception{
        user_dto.setEmail(null);
        HashMap newUser = new HashMap();
        newUser.put("user",user_dto);
        newUser.put("model",model);
        newUser.put("result",result);
        mockMvc.perform(post("/adduser").flashAttrs(newUser)).andExpect(status().isOk()).andExpect(view().name("index"));
    }

    @Test
    public void testNullUser() throws Exception{
        user_dto.setName(null);
        user_dto.setEmail(null);
        HashMap newUser = new HashMap();
        newUser.put("user",user_dto);
        newUser.put("model",model);
        newUser.put("result",result);
        mockMvc.perform(post("/adduser").flashAttrs(newUser)).andExpect(status().isOk()).andExpect(view().name("index"));
    }

    @Test
    public void testInvalidRequestUser() throws Exception{
        mockMvc.perform(get("/adduser")).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void testAddUserError() throws Exception{
        HashMap newUser = new HashMap();
        newUser.put("user",user);
        newUser.put("model",model);
        newUser.put("result",result);
        mockMvc.perform(post("/adduser").flashAttrs(newUser)).andExpect(status().isInternalServerError());
    }

    //Test Additional Features
    @Test
    public void testUserDetails() throws Exception{
        try{
            mockMvc.perform(get("/details/" + id)).andExpect(status().isOk()).andExpect(view().name("details"));
        }catch (Exception e){
            assertThat(e).hasMessageContaining(ID_NOT_FOUND);
        }
    }
}
