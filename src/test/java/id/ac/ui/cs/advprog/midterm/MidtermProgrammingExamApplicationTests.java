package id.ac.ui.cs.advprog.midterm;

import static org.assertj.core.api.Assertions.*;

import id.ac.ui.cs.advprog.midterm.controller.UserController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class MidtermProgrammingExamApplicationTests {
	@Autowired
	private UserController controller;

	@Test
	void contextLoads() {
		MidtermProgrammingExamApplication.main(new String[]{});
		assertThat(controller).isNotNull();
	}

}
