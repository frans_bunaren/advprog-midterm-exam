package id.ac.ui.cs.advprog.midterm.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class UserWithDTOTest {
    @Mock
    private UserWithDTO user;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        user = new UserWithDTO();
        user.setId(1);
        user.setEmail("email@email.com");
        user.setName("AdvProgMaster");
    }

    @Test
    public void testSetName(){
        user.setName("Fransiscus");
        assertThat(user.getName()).isEqualTo("Fransiscus");
    }

    @Test
    public void testSetEmail(){
        user.setEmail("email@123.com");
        assertThat(user.getEmail()).isEqualTo("email@123.com");
    }

    @Test
    public void testSetID(){
        user.setId(0);
        assertThat(user.getId()).isEqualTo(0);
    }

    @Test
    public void testToString(){
        assertThat(user.toString()).isEqualTo("1");
    }

    @Test
    public void testWebsite(){
        user.setWebsite("http://bunaren.com");
        assertThat(user.getWebsite()).isEqualTo("http://bunaren.com");
    }
}
