package id.ac.ui.cs.advprog.midterm.core;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="`User`")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank(message = "Name is mandatory")
    private String name;

    @NotBlank(message = "Email is mandatory")
    private String email;

    private String website;

    // standard constructors / setters / getters / toString
    public void setId(long id){
        this.id = id;
    }

    public long getId(){
        return this.id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getEmail(){
        return this.email;
    }

    public String getWebsite(){
        return this.website;
    }

    public void setWebsite(String url){
        this.website = url;
    }

    @Override
    public String toString() {
        return Long.toString(this.id);
    }
}