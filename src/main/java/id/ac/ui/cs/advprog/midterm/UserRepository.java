package id.ac.ui.cs.advprog.midterm;

import id.ac.ui.cs.advprog.midterm.core.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {}