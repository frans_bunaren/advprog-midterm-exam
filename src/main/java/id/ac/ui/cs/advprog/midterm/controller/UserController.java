package id.ac.ui.cs.advprog.midterm.controller;
import id.ac.ui.cs.advprog.midterm.UserRepository;
import id.ac.ui.cs.advprog.midterm.core.User;
import id.ac.ui.cs.advprog.midterm.core.UserWithDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class UserController {
    private static final String USER_NAME = "users";
    private static final String INDEX_TEMPLATE = "index";
    private static final String ID_NOT_FOUND = "Invalid user Id:";

    @Autowired
    private UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/signup")
    public String showSignUpForm(@ModelAttribute("user") UserWithDTO user) {
        return "add-user";
    }

    @PostMapping("/adduser")
    public String addUser(@ModelAttribute("user") @Valid UserWithDTO userwithdto, BindingResult result, Model model) {
        try{
            User user = new User();
            BeanUtils.copyProperties(userwithdto, user);

            userRepository.save(user);
            model.addAttribute(USER_NAME, userRepository.findAll());
            return INDEX_TEMPLATE;
        }catch (Exception e){
            return "add-user";
        }
    }

    // additional CRUD methods
    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(ID_NOT_FOUND + id));
        model.addAttribute("user", user);
        return "update-user";
    }

    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") long id, @ModelAttribute("user") @Valid UserWithDTO userwithdto, BindingResult result, Model model) {
        try {
            User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(ID_NOT_FOUND + id));
            BeanUtils.copyProperties(userwithdto, user);
            userRepository.save(user);
            model.addAttribute(USER_NAME, userRepository.findAll());
            return INDEX_TEMPLATE;
        }catch (Exception e){
            return "update-user";
        }
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(ID_NOT_FOUND + id));
        userRepository.delete(user);
        model.addAttribute(USER_NAME, userRepository.findAll());
        return INDEX_TEMPLATE;
    }

    //Additional Feature
    //View User Detailed Information
    @GetMapping("/details/{id}")
    public String userDetails(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(ID_NOT_FOUND + id));
        model.addAttribute("user", user);
        return "details";

    }
}